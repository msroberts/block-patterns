import { IModel, IModelMap, IPathMap, IPoint, models } from "makerjs";
import { dot } from "./dot";
import { halfPoint } from "./points";

export class WaistDart implements IModel {
  models: IModelMap = {};
  paths: IPathMap = {};

  constructor(points: IPoint[]) {
    this.models.dart = new models.ConnectTheDots(true, points.slice(0, 4));
    points[4] = points[0];
    for (let i = 1; i < points.length; i++) {
      this.paths[`dot${i}`] = dot(halfPoint(points[i - 1], points[i]));
    }
  }
}
