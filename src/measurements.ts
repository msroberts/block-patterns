export interface IMeasurements {
  bust: number;
  waist: number;
  hip: number;
  chest: number;
  arm: number;
  shoulderWidth: number;
  bustHeight: number;
  waistHeight: number;
  sideLength: number,
  rise: number,
  kneeWidth: number,
  ease: number;
}

export const defaultMeasurements: IMeasurements = {
  bust: 0,
  waist: 0,
  hip: 0,
  chest: 0,
  arm: 0,
  shoulderWidth: 0,
  bustHeight: 0,
  waistHeight: 0,
  sideLength: 0,
  rise: 0,
  kneeWidth: 0,
  ease: 3,
};

export function missingMeasurment(measurements: IMeasurements) {
  for (const [key, value] of Object.entries(measurements)) {
    if (value <= 0) return key;
  }

  return null;
}
