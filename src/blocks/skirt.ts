import {
  angle,
  IModel,
  IModelMap,
  IPathMap,
  IPoint,
  measure,
  models,
  paths,
  point,
} from "makerjs";
import { Curve } from "../helpers/curve";
import { dot } from "../helpers/dot";
import { ANGLE_DOWN, pointAtAngle } from "../helpers/points";
import { IMeasurements } from "../measurements";
import {
  getBackDartWidth,
  getBustWidth,
  getFrontDartWidth,
  getOuterWaistPoint,
  getSideDartWidth,
  getWaistReduction,
  HIP_LENGTH,
} from "./shared";

export class Skirt implements IModel {
  models: IModelMap = {};
  paths: IPathMap = {};
  layer = "green";

  constructor(measurements: IMeasurements, skirtLength = 24) {
    const { waist, hip, shoulderWidth, ease } = measurements;
    const { PI } = Math;

    const totalWidth = (hip + ease) / 2;
    const waistReduction = hip - waist;
    const frontDartWidth = waistReduction / 18;
    const sideDartWidth = (waistReduction / 2 - frontDartWidth) * (2 / 3);
    const backDartWidth = (waistReduction / 2 - frontDartWidth) / 3;

    const bodiceMaxWidth = getOuterWaistPoint(measurements);
    const bodiceWaistReduction = getWaistReduction(measurements);
    const frontWidth =
      bodiceMaxWidth -
      getFrontDartWidth(bodiceWaistReduction) -
      getSideDartWidth(bodiceWaistReduction);
    const centerFrontWidth =
      getBustWidth(measurements) / 2 -
      getFrontDartWidth(bodiceWaistReduction) / 2;

    const hipPoint: IPoint = [
      totalWidth - (frontWidth + sideDartWidth / 2 + frontDartWidth),
      -HIP_LENGTH,
    ];

    const backDartDepth = HIP_LENGTH / 2;

    const frontWaistPoint: IPoint = [totalWidth, 0];
    const backWaistPoint = [0, 0];
    const backCenterWidth =
      shoulderWidth / 4 - getBackDartWidth(bodiceWaistReduction) / 2;

    const backDartInnerPoint = point.add(backWaistPoint, [backCenterWidth, 0]);
    const backDartLowerPoint = point.add(backDartInnerPoint, [
      backDartWidth / 2,
      -backDartDepth,
    ]);
    const backDartOuterPoint = point.add(backDartInnerPoint, [
      backDartWidth,
      0,
    ]);

    const sideDartInnerPoint = [hipPoint[0] - sideDartWidth / 2, 0];
    const sideDartOuterPoint = [hipPoint[0] + sideDartWidth / 2, 0];

    const waistlineCurveLength =
      measure.pointDistance(backDartOuterPoint, sideDartInnerPoint) / 3;
    const waistlineCurvePoint = point.add(backDartOuterPoint, [
      waistlineCurveLength,
      0,
    ]);
    const waistlineCurveAngle =
      PI / 2 - angle.ofPointInRadians(backDartLowerPoint, backDartOuterPoint);
    const raisedWaistline = point.fromSlopeIntersection(
      new paths.Line(backDartLowerPoint, backDartOuterPoint),
      new paths.Line(
        waistlineCurvePoint,
        pointAtAngle(
          waistlineCurvePoint,
          -waistlineCurveAngle,
          waistlineCurveLength
        )
      )
    );
    raisedWaistline[0] = 0;

    const backDartInnerPointRaised = point.add(
      backDartInnerPoint,
      raisedWaistline
    );
    const backDartOuterPointRaised = point.add(
      backDartOuterPoint,
      raisedWaistline
    );
    this.models.backWaistline = new Curve([
      {
        origin: backWaistPoint,
        angle: 0,
        distance: waistlineCurveLength,
      },
      {
        origin: backDartInnerPointRaised,
        angle: waistlineCurveAngle,
        distance: waistlineCurveLength,
      },
    ]);
    this.models.backWaistDartInner = new Curve([
      {
        origin: backDartInnerPointRaised,
        angle: ANGLE_DOWN + waistlineCurveAngle,
        distance: backDartDepth / 9,
      },
      {
        origin: backDartLowerPoint,
        angle: ANGLE_DOWN,
        distance: backDartDepth / 3,
      },
    ]);
    this.models.backWaistDartOuter = new Curve([
      {
        origin: backDartOuterPointRaised,
        angle: ANGLE_DOWN - waistlineCurveAngle,
        distance: backDartDepth / 9,
      },
      {
        origin: backDartLowerPoint,
        angle: ANGLE_DOWN,
        distance: backDartDepth / 3,
      },
    ]);

    const sideDartInnerPointRaised = point.add(
      sideDartInnerPoint,
      raisedWaistline
    );
    const sideDartOuterPointRaised = point.add(
      sideDartOuterPoint,
      raisedWaistline
    );
    this.models.sideWaistline = new Curve([
      {
        origin: backDartOuterPointRaised,
        angle: -waistlineCurveAngle,
        distance: waistlineCurveLength,
      },
      {
        origin: sideDartInnerPointRaised,
        angle: waistlineCurveAngle,
        distance: waistlineCurveLength,
      },
    ]);
    this.models.sideWaistDartInner = new Curve([
      {
        origin: sideDartInnerPointRaised,
        angle: ANGLE_DOWN + waistlineCurveAngle,
        distance: backDartDepth / 9,
      },
      {
        origin: hipPoint,
        angle: ANGLE_DOWN,
        distance: backDartDepth,
      },
    ]);
    this.models.sideWaistDartOuter = new Curve([
      {
        origin: sideDartOuterPointRaised,
        angle: ANGLE_DOWN - waistlineCurveAngle,
        distance: backDartDepth / 9,
      },
      {
        origin: hipPoint,
        angle: ANGLE_DOWN,
        distance: backDartDepth,
      },
    ]);

    const lowerFrontWaistPoint = point.add(
      frontWaistPoint,
      raisedWaistline,
      true
    );
    this.models.frontWaistline = new Curve([
      {
        origin: sideDartOuterPointRaised,
        angle: -waistlineCurveAngle,
        distance: waistlineCurveLength * 2,
      },
      {
        origin: lowerFrontWaistPoint,
        angle: 0,
        distance: waistlineCurveLength * 2,
      },
    ]);

    this.models.seam = new models.ConnectTheDots(false, [
      lowerFrontWaistPoint,
      point.add(frontWaistPoint, [0, -skirtLength]),
      point.add(backWaistPoint, [0, -skirtLength]),
      backWaistPoint,
    ]);

    this.paths.side = new paths.Line(hipPoint, [hipPoint[0], -skirtLength]);
    this.paths.hipPoint = dot(hipPoint);
    this.paths.backDartPoint = dot(backDartLowerPoint);

    const frontDartPoints: IPoint[] = [
      [totalWidth - centerFrontWidth, lowerFrontWaistPoint[1]],
      [totalWidth - centerFrontWidth - frontDartWidth / 2, -HIP_LENGTH / 3],
      [totalWidth - centerFrontWidth - frontDartWidth, lowerFrontWaistPoint[1]],
    ];
    this.models.frontDart = new models.ConnectTheDots(false, frontDartPoints);
    frontDartPoints.forEach((p, i) => (this.paths[`frontDart${i}`] = dot(p)));
  }
}
