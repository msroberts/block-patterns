import {
  IModel,
  IModelMap,
  IPathMap,
  IPoint,
  angle,
  measure,
  models,
  paths,
  point,
} from "makerjs";
import { IMeasurements } from "../measurements";
import { Curve } from "../helpers/curve";
import { dot } from "../helpers/dot";
import { pointAtAngle, pointAtDistance, pointAtXY } from "../helpers/points";

export class Pants implements IModel {
  public models: IModelMap = {};
  public paths: IPathMap = {};
  public layer = "teal";

  constructor(
    measurements: IMeasurements,
    additionalWidth = -1,
    fitAmount = -1
  ) {
    const { hip, waist, rise, ease } = measurements;
    if (additionalWidth < 0) additionalWidth = 2.36;
    if (fitAmount < 0) fitAmount = 1.57;

    const { PI } = Math;

    const patternTopWidth = (hip + additionalWidth) / 2;
    const origin: IPoint = [0, 0];
    const pointX = point.add(origin, [0, -rise]);
    let pointZ = point.add(origin, [-patternTopWidth, 0]);
    const pointY: IPoint = pointAtXY(pointZ, pointX);

    const pointS = point.average(origin, pointZ);
    const hipPoint = point.add(pointS, [0, -Math.min(8.66, rise - 2)]);

    const rotationAngle = Math.asin((0.6 / (hipPoint[0] - pointY[0])) * -2);
    const rotate = getRotationFunction(
      hipPoint,
      angle.toDegrees(rotationAngle)
    );
    pointZ = rotate(pointZ);
    const adjustedHipPoint = rotate(pointAtXY(pointY, hipPoint));
    const adjustedPointS = rotate(pointS);
    const upperHipPoint = pointAtDistance(
      hipPoint,
      point.average(pointZ, origin),
      Math.abs(hipPoint[1])
    );

    const pointF = point.add(pointX, [Math.max(hip / 10 - 2, 1.6), 0]);
    const pointB = point.add(pointF, [-((hip * 3) / 4 - fitAmount), -0.6]);

    const creaseLine = (pointF[0] - hipPoint[0]) / 2;

    const pointM = point.add(pointF, [-creaseLine, 0]);
    const pointN = point.add(pointM, [-creaseLine * 2, 0]);

    this.paths.O = dot(origin);
    this.paths.S = dot(pointS);
    this.paths.Z = dot(pointZ);
    this.paths.B = dot(pointB);
    this.paths.Y = dot(pointY);
    this.paths.HP = dot(hipPoint);
    this.paths.WP = dot(upperHipPoint);
    this.paths.X = dot(pointX);
    this.paths.F = dot(pointF);
    this.paths.M = dot(pointM);
    this.paths.N = dot(pointN);

    this.models.waistLine = new models.ConnectTheDots(false, [
      pointZ,
      upperHipPoint,
      origin,
    ]);
    this.models.backSeam = new Curve([
      {
        origin: pointZ,
        angle: -PI / 2 + rotationAngle,
        distance: measure.pointDistance(pointZ, pointY) * (2 / 3),
      },
      {
        origin: pointB,
        angle: angle.ofPointInRadians(pointY, pointB),
        distance: measure.pointDistance(pointB, pointY),
      },
    ]);
    this.models.frontSeam = new Curve([
      {
        origin,
        angle: -PI / 2,
        distance: measure.pointDistance(origin, pointX) / 2,
      },
      {
        origin: pointF,
        angle: -PI / 4,
        distance: measure.pointDistance(pointX, pointF),
      },
    ]);

    const dartDepth = Math.abs(hipPoint[1]) * (2 / 3);
    const waistReduction = hip - waist + ease;

    const innerBackWaistPoint = pointAtDistance(
      pointZ,
      upperHipPoint,
      waistReduction / 16
    );
    const backInnerPoint = pointAtAngle(
      pointZ,
      -PI / 2 + rotationAngle,
      dartDepth
    );
    this.models.backInnerLine = new Curve([
      {
        origin: innerBackWaistPoint,
        angle: 0,
        distance: 0,
      },
      {
        origin: backInnerPoint,
        angle: angle.ofPointInRadians(innerBackWaistPoint, backInnerPoint),
        distance: dartDepth / 2,
      },
      {
        origin: pointB,
        angle: angle.ofPointInRadians(pointY, pointB),
        distance: measure.pointDistance(pointB, pointY) * (3 / 4),
      },
    ]);

    const innerFrontWaistPoint = point.add(
      origin,
      [waistReduction / 16, 2 / 5],
      true
    );
    const frontInnerPoint = pointAtAngle(origin, -PI / 2, dartDepth);
    this.models.frontInnerLine = new Curve([
      {
        origin: innerFrontWaistPoint,
        angle: 0,
        distance: 0,
      },
      {
        origin: frontInnerPoint,
        angle: angle.ofPointInRadians(innerFrontWaistPoint, frontInnerPoint),
        distance: dartDepth / 3,
      },
      {
        origin: pointF,
        angle: -PI / 6,
        distance: measure.pointDistance(pointX, pointF) * (2 / 3),
      },
    ]);
  }
}

function getRotationFunction(origin: IPoint, angleInDegrees: number) {
  return (p: IPoint) => point.rotate(p, angleInDegrees, origin);
}
