let hiddenLink: HTMLAnchorElement;

function getLinkElement() {
  if (hiddenLink) return hiddenLink;

  hiddenLink = document.createElement("a");
  hiddenLink.style.display = "none";
  document.body.appendChild(hiddenLink);

  return hiddenLink;
}

export function downloadData(data: BlobPart, filename: string) {
  const blob = new Blob([data], { type: "octet/stream" });
  const url = URL.createObjectURL(blob);

  const a = getLinkElement();
  a.href = url;
  a.download = filename;

  a.click();
  URL.revokeObjectURL(url);
}
