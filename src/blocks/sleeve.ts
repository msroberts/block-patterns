import {
  angle,
  IModel,
  IModelMap,
  IPathMap,
  IPoint,
  measure,
  models,
  paths,
  point,
} from "makerjs";
import { Curve } from "../helpers/curve";
import { dot } from "../helpers/dot";
import { halfPoint, pointAtDistance } from "../helpers/points";
import { IMeasurements } from "../measurements";
import { getArmholeDepth } from "./shared";

export class Sleeve implements IModel {
  models: IModelMap = {};
  paths: IPathMap = {};
  layer = "orange";

  constructor(measurements: IMeasurements, sleeveLength = 5) {
    const { arm, ease } = measurements;

    const armCrownDepth = arm / 4 + 2.125;
    const totalWidth = getArmholeDepth(measurements) * 2;

    sleeveLength = Math.max(sleeveLength, armCrownDepth + 1.5);

    const backUnderArmPoint: IPoint = [0, -armCrownDepth];
    const topArmPoint: IPoint = [totalWidth / 2, 0];
    const frontUnderArmPoint = point.add(backUnderArmPoint, [totalWidth, 0]);

    const backLowerSleevePoint: IPoint = [
      backUnderArmPoint[0] + ease / 4,
      -sleeveLength,
    ];
    const frontLowerSleevePoint: IPoint = [
      frontUnderArmPoint[0] - ease / 4,
      -sleeveLength,
    ];

    const middlePoint: IPoint = [topArmPoint[0], frontUnderArmPoint[1]];
    this.paths.middleDot = dot(middlePoint);

    const innerBackHalfPoint = halfPoint(backUnderArmPoint, topArmPoint);
    const backHalfPoint = pointAtDistance(
      middlePoint,
      innerBackHalfPoint,
      measure.pointDistance(middlePoint, innerBackHalfPoint) + 1 / 2
    );
    this.paths.backDot = dot(backHalfPoint);

    const frontHalfPoint = halfPoint(frontUnderArmPoint, topArmPoint);
    this.paths.frontDot = dot(frontHalfPoint);

    const backSleeveSlopeAngle = angle.ofPointInRadians(
      backUnderArmPoint,
      backHalfPoint
    );
    const frontSleeveSlopeAngle = angle.ofPointInRadians(
      frontUnderArmPoint,
      topArmPoint
    );
    const sleeveCurveLength = arm / 8;

    this.models.crown = new Curve([
      { origin: backUnderArmPoint, angle: backSleeveSlopeAngle, distance: 0 },
      {
        origin: backHalfPoint,
        angle: backSleeveSlopeAngle,
        distance: sleeveCurveLength,
      },
      { origin: topArmPoint, angle: 0, distance: sleeveCurveLength },
      {
        origin: frontHalfPoint,
        angle: Math.PI + frontSleeveSlopeAngle * (4 / 5),
        distance: sleeveCurveLength,
      },
      {
        origin: frontUnderArmPoint,
        angle:
          angle.ofPointInRadians(frontUnderArmPoint, frontLowerSleevePoint) +
          (backSleeveSlopeAngle -
            angle.ofPointInRadians(backUnderArmPoint, backLowerSleevePoint)),
        distance: sleeveCurveLength,
      },
    ]);

    const middleHemPoint: IPoint = [topArmPoint[0], frontLowerSleevePoint[1]];
    this.paths.middle = new paths.Line(topArmPoint, middleHemPoint);

    this.models.seam = new models.ConnectTheDots(false, [
      frontUnderArmPoint,
      frontLowerSleevePoint,
      backLowerSleevePoint,
      backUnderArmPoint,
    ]);
  }
}
