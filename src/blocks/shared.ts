import { IPoint, paths, point } from "makerjs";
import { pointAtAngle } from "../helpers/points";
import { IMeasurements } from "../measurements";

export const HIP_LENGTH = 9;

export const SHOULDER_SLOPE_ANGLE = -Math.atan(1 / 3);

export const BALANCE_ANGLE = -Math.atan(1 / 6);

export const LOWER_WAISTLINE = 3 / 8;

export function getNeckWidth({ bust }: IMeasurements) {
  return Math.max(bust / 16 + 1 / 2, 2.75);
}

export function getShoulderPoints(measurements: IMeasurements) {
  const { shoulderWidth } = measurements;
  const upperShoulderPoint: IPoint = [getNeckWidth(measurements), 0];

  const lowerShoulderPoint = point.fromSlopeIntersection(
    new paths.Line(
      upperShoulderPoint,
      pointAtAngle(upperShoulderPoint, SHOULDER_SLOPE_ANGLE, 1)
    ),
    new paths.Line([shoulderWidth / 2, 0], [shoulderWidth / 2, 1])
  );

  return {
    upperShoulderPoint,
    lowerShoulderPoint,
  };
}

export function getArmholeDepth({ arm, ease }: IMeasurements) {
  return arm / 2 + ease / 3;
}

export function getUnderArmWidth({ bust, chest, ease }: IMeasurements) {
  return Math.max(chest + ease, bust) / 4;
}

export function getOuterWaistPoint(measurements: IMeasurements) {
  const { hip } = measurements;
  const underArmWidth = getUnderArmWidth(measurements) * 4;

  return (underArmWidth + hip) / 8;
}

export function getWaistReduction(measurements: IMeasurements) {
  const { waist, ease } = measurements;
  const outerWaistWidth = getOuterWaistPoint(measurements) * 4;

  return outerWaistWidth - waist - ease;
}

export function getBackDartWidth(waistReduction: number) {
  return waistReduction / 6;
}

export function getSideDartWidth(waistReduction: number) {
  return waistReduction / 12;
}

export function getFrontDartWidth(waistReduction: number) {
  return waistReduction / 6;
}

export function getBustWidth({ bust }: IMeasurements) {
  return bust / 4.5;
}

export function getHipWidth({ hip, ease }: IMeasurements) {
  return hip / 4 + ease / 6;
}

export function getPatternWidth({ bust, chest, hip, ease }: IMeasurements) {
  return Math.max(bust / 2, (chest + ease) / 2, hip / 2 + ease / 3);
}
