import { get, set } from "idb-keyval";
import { defaultMeasurements, IMeasurements } from "./measurements";

export const MEASUREMENTS_KEY = "measurements";

export async function getMeasurements() {
  const measurements = {
    ...defaultMeasurements,
    ...(await get<IMeasurements>(MEASUREMENTS_KEY)),
  };

  return measurements;
}

export function setMeasurements(measurements: IMeasurements) {
  return set(MEASUREMENTS_KEY, measurements);
}
