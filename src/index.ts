import { exporter } from "makerjs";
import { Block } from "./blocks/block";
import { downloadData } from "./export";
import { IMeasurements } from "./measurements";
import { getMeasurements, setMeasurements } from "./settings";

const jsonInput = document.getElementById("jsonInput") as HTMLTextAreaElement;

async function load() {
  const measurements = await getMeasurements();
  await setMeasurements(measurements);

  jsonInput.value = JSON.stringify(measurements, null, 2);
  jsonInput.addEventListener("change", () => updateMeasurments().then());

  generateBlock(measurements);

  (
    document.getElementById("downloadSVG") as HTMLButtonElement
  ).addEventListener("click", () => downloadBlock("svg"));
  (
    document.getElementById("downloadJSON") as HTMLButtonElement
  ).addEventListener("click", () => downloadBlock("json"));
  (document.getElementById("downloadJS") as HTMLButtonElement).addEventListener(
    "click",
    () => downloadBlock("js")
  );
}

async function updateMeasurments() {
  const measurements: IMeasurements = JSON.parse(jsonInput.value);

  if (!measurements) {
    console.warn("No measurements found.");
    return;
  }

  await setMeasurements(measurements);

  generateBlock(measurements);
}

function generateBlock(measurements: IMeasurements) {
  const outputDiv = document.getElementById("output") as HTMLDivElement;
  outputDiv.innerHTML = exporter.toSVG(new Block(measurements));
}

function downloadBlock(type: "svg" | "json" | "js") {
  const measurements: IMeasurements = JSON.parse(jsonInput.value);
  const block = new Block(measurements);

  switch (type) {
    case "json":
      downloadData(exporter.toJson(block), "block.json");
      break;

    case "js":
      downloadData(exporter.toJscadScript(block), "block.js");
      break;

    default: // SVG
      downloadData(exporter.toSVG(block), "block.svg");
      break;
  }
}

load()
  .then()
  .catch((err) => console.error(err));
