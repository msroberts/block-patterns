import { IMeasure, IModel, IModelMap, layout, paths } from "makerjs";

const DOT_SIZE = 1 / 32;

export class Grid implements IModel {
  models: IModelMap = {};
  layer = "gray";

  constructor({ high, low }: IMeasure) {
    const { floor } = Math;

    const grid = layout.cloneToGrid(
      new paths.Circle(DOT_SIZE / 2),
      floor(high[0] - low[0]) + 3,
      floor(high[1] - low[1]) + 3,
      1 - DOT_SIZE
    );

    grid.origin = [-1, floor(low[1]) - 1];
    this.models.grid = grid;
  }
}
