import {
  angle,
  IModel,
  IModelMap,
  IPathMap,
  IPoint,
  measure,
  models,
  paths,
  point,
} from "makerjs";
import { Curve } from "../helpers/curve";
import { Dart } from "../helpers/dart";
import { dot } from "../helpers/dot";
import { ANGLE_DOWN, pointAtAngle, pointAtDistance } from "../helpers/points";
import { WaistDart } from "../helpers/waistDart";
import { IMeasurements } from "../measurements";
import {
  BALANCE_ANGLE,
  getArmholeDepth,
  getBackDartWidth,
  getHipWidth,
  getOuterWaistPoint,
  getShoulderPoints,
  getSideDartWidth,
  getUnderArmWidth,
  getWaistReduction,
  HIP_LENGTH,
  LOWER_WAISTLINE,
  SHOULDER_SLOPE_ANGLE,
} from "./shared";

export class Back implements IModel {
  public models: IModelMap = {};
  public paths: IPathMap = {};
  public layer = "blue";

  constructor(measurements: IMeasurements) {
    const { bustHeight, waistHeight, shoulderWidth } = measurements;
    const { PI } = Math;

    const centerBackNecklinePoint: IPoint = [0, -1];
    const { upperShoulderPoint, lowerShoulderPoint } =
      getShoulderPoints(measurements);
    const shoulderLength = measure.pointDistance(
      upperShoulderPoint,
      lowerShoulderPoint
    );

    this.models.neckline = new Curve([
      {
        origin: centerBackNecklinePoint,
        angle: 0,
        distance: upperShoulderPoint[0] / 2,
      },
      {
        origin: upperShoulderPoint,
        angle: SHOULDER_SLOPE_ANGLE * 2 - PI / 2,
        distance: centerBackNecklinePoint[1] / 2,
      },
    ]);

    const shoulderDartWidth = 3 / 8;

    const unAdjustedInnerShoulderDartPoint = pointAtAngle(
      upperShoulderPoint,
      SHOULDER_SLOPE_ANGLE,
      shoulderLength / 2
    );

    const extendedShoulderPoint = pointAtAngle(
      lowerShoulderPoint,
      0,
      shoulderDartWidth
    );

    const armholeDepth = getArmholeDepth(measurements);

    const middleArmholePoint = point.add(lowerShoulderPoint, [
      -shoulderDartWidth / 2,
      -armholeDepth / 2,
    ]);

    const underArmWidth = getUnderArmWidth(measurements);
    const underArmPoint: IPoint = [
      underArmWidth,
      middleArmholePoint[1] - armholeDepth / 2,
    ];

    const shoulderDart = new Dart({
      outerPoint0: upperShoulderPoint,
      innerPoint0: unAdjustedInnerShoulderDartPoint,
      basePoint: pointAtDistance(
        pointAtAngle(
          unAdjustedInnerShoulderDartPoint,
          SHOULDER_SLOPE_ANGLE,
          shoulderDartWidth / 2
        ),
        [shoulderWidth / 4, underArmPoint[1]],
        armholeDepth / 2
      ),
      innerPoint1: pointAtAngle(
        unAdjustedInnerShoulderDartPoint,
        SHOULDER_SLOPE_ANGLE,
        shoulderDartWidth
      ),
      outerPoint1: extendedShoulderPoint,
    });

    this.models.shoulder = shoulderDart;

    this.models.armhole = new Curve([
      {
        origin: extendedShoulderPoint,
        angle:
          ANGLE_DOWN +
          angle.ofPointInRadians(
            shoulderDart.getPoints().adjustedInnerPoint1,
            extendedShoulderPoint
          ),
        distance: 1 / 2,
      },
      {
        origin: middleArmholePoint,
        angle: ANGLE_DOWN,
        distance: armholeDepth / 4,
      },
      {
        origin: underArmPoint,
        angle: BALANCE_ANGLE,
        distance: (underArmPoint[0] - middleArmholePoint[0]) / 2,
      },
    ]);

    this.paths.armholeDot = dot(middleArmholePoint);

    const waistReduction = getWaistReduction(measurements);
    const outerWaistPoint: IPoint = [
      getOuterWaistPoint(measurements) - getSideDartWidth(waistReduction),
      -waistHeight,
    ];
    const outerHipPoint: IPoint = [
      getHipWidth(measurements),
      -waistHeight - HIP_LENGTH,
    ];
    this.models.side = new Curve([
      {
        origin: underArmPoint,
        angle: BALANCE_ANGLE - PI / 2,
        distance: (underArmPoint[1] + waistHeight) / 3,
      },
      {
        origin: outerWaistPoint,
        angle: ANGLE_DOWN,
        distance: HIP_LENGTH / 4,
      },
      {
        origin: outerHipPoint,
        angle: ANGLE_DOWN,
        distance: HIP_LENGTH / 3,
      },
    ]);

    const backDartUpperPoint: IPoint = [shoulderWidth / 4, -bustHeight];
    const backDartInnerPoint: IPoint = [
      backDartUpperPoint[0] - getBackDartWidth(waistReduction) / 2,
      outerWaistPoint[1],
    ];
    const backDartOuterPoint = point.add(backDartInnerPoint, [
      getBackDartWidth(waistReduction),
      0,
    ]);
    const backDartLowerPoint: IPoint = [
      backDartUpperPoint[0],
      outerHipPoint[1] + HIP_LENGTH / 4,
    ];
    this.paths.dartHipPoint = dot([backDartLowerPoint[0], outerHipPoint[1]]);

    this.models.backWaistDart = new WaistDart([
      backDartInnerPoint,
      backDartUpperPoint,
      backDartOuterPoint,
      backDartLowerPoint,
    ]);

    const centerWaistPoint: IPoint = [0, outerWaistPoint[1]];
    this.paths.innerWaistLine = new paths.Line(
      centerWaistPoint,
      backDartInnerPoint
    );
    this.paths.outerWaistLine = new paths.Line(
      backDartOuterPoint,
      point.add(outerWaistPoint, [0, -LOWER_WAISTLINE / 2])
    );
    this.paths.waistDot = dot(outerWaistPoint);

    this.models.seam = new models.ConnectTheDots(false, [
      outerHipPoint,
      [0, outerHipPoint[1]],
      centerBackNecklinePoint,
    ]);
  }
}
