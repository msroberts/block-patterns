import { IPoint, paths } from "makerjs";

export const DOT_WIDTH = 1 / 8;

export function dot(origin: IPoint) {
  return new paths.Circle(origin, DOT_WIDTH / 2);
}
