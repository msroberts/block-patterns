import { angle, IPoint, point } from "makerjs";

export const ANGLE_DOWN = -Math.PI / 2;

export function pointAtAngle(
  start: IPoint,
  angleInRadians: number,
  distance: number
): IPoint {
  return point.add(start, point.fromPolar(angleInRadians, distance));
}

export function pointAtDistance(
  start: IPoint,
  direction: IPoint,
  distance: number
): IPoint {
  return pointAtAngle(
    start,
    angle.ofPointInRadians(start, direction),
    distance
  );
}

export function halfPoint(point0: IPoint, point1: IPoint): IPoint {
  return [(point0[0] + point1[0]) / 2, (point0[1] + point1[1]) / 2];
}

export function pointAtXY(pointX: IPoint, pointY: IPoint): IPoint {
  return [pointX[0], pointY[1]];
}
