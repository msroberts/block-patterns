import { IModel, IModelMap, measure, model, unitType } from "makerjs";
import { Grid } from "../helpers/grid";
import { IMeasurements, missingMeasurment } from "../measurements";
import { Back } from "./back";
import { Front } from "./front";
import { Skirt } from "./skirt";
import { Sleeve } from "./sleeve";
import { Pants } from "./pants";

export class Block implements IModel {
  models: IModelMap = {};
  units = unitType.Inch;

  constructor(measurements: IMeasurements) {
    const missing = missingMeasurment(measurements);
    if (missing) {
      throw new Error(`Missing measurement: ${missing}`);
    }

    const { ceil } = Math;

    const back = new Back(measurements);
    const front = new Front(measurements);

    const frontX = ceil(measure.modelExtents(front).high[0] + 1 / 4);

    const sleeve = new Sleeve(measurements);
    model.move(sleeve, [frontX, 0]);

    const skirt = new Skirt(measurements);
    model.move(skirt, [frontX, measure.modelExtents(sleeve).low[1] - 1]);

    const pants = new Pants(measurements);
    model.move(pants, [
      ceil(
        measure.modelExtents(skirt).high[0] + measure.modelExtents(pants).width
      ),
      0,
    ]);

    this.models = {
      back,
      front,
      sleeve,
      skirt,
      pants,
    };

    const grid = new Grid(measure.modelExtents(this));
    this.models.grid = grid;
  }
}
