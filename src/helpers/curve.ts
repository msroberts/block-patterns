import { IModel, IModelMap, IPathMap, IPoint, models, paths } from "makerjs";
import { dot } from "./dot";
import { pointAtAngle } from "./points";

export interface ICurvePoint {
  origin: IPoint;
  angle: number;
  distance: number;
}

export class Curve implements IModel {
  models: IModelMap = {};
  paths: IPathMap = {};

  constructor(points: ICurvePoint[], showHandles = false) {
    if (points.length < 2) return;

    for (let i = 1; i < points.length; i++) {
      const start = points[i - 1];
      const end = points[i];

      const handle0 = pointAtAngle(start.origin, start.angle, start.distance);
      const handle1 = pointAtAngle(end.origin, end.angle, -end.distance);

      this.models[`curve${i}`] = new models.BezierCurve(
        start.origin,
        handle0,
        handle1,
        end.origin
      );

      if (showHandles) {
        this.paths[`dot0${i}`] = dot(handle0);
        this.paths[`line0${i}`] = new paths.Line(start.origin, handle0);
        this.paths[`dot1${i}`] = dot(handle1);
        this.paths[`line1${i}`] = new paths.Line(end.origin, handle1);
      }
    }
  }
}
