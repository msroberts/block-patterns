export function solveTriangleSAS(
  side0Length: number,
  angleInRadians: number,
  side1Length: number
) {
  const { cos, sqrt } = Math;
  const oppositeSideLength = sqrt(
    side0Length ** 2 +
      side1Length ** 2 -
      2 * side0Length * side1Length * cos(angleInRadians)
  );
  return oppositeSideLength;
}
