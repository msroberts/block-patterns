import nodeResolve from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";

/**
 * @type {import('rollup').RollupOptions}
 */
const config = {
  input: "src/index.ts",
  plugins: [nodeResolve(), typescript()],
  external: ["makerjs"],
  output: {
    file: "public/bundle.js",
    format: "iife",
    globals: {
      makerjs: "makerjs",
    },
    sourcemap: true,
  },
};

export default config;
