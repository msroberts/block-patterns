import {
  angle,
  IModel,
  IModelMap,
  IPathMap,
  IPoint,
  measure,
  models,
  paths,
  point,
} from "makerjs";
import { solveTriangleSAS } from "./angles";
import { dot } from "./dot";
import { halfPoint, pointAtAngle, pointAtDistance } from "./points";

export interface IDartPoints {
  outerPoint0: IPoint;
  innerPoint0: IPoint;
  basePoint: IPoint;
  innerPoint1: IPoint;
  outerPoint1: IPoint;
}

export interface IAdjustedDartPoints extends IDartPoints {
  adjustedInnerPoint0: IPoint;
  adjustedInnerPoint1: IPoint;
  bisector: IPoint;
}

export class Dart implements IModel {
  models: IModelMap;
  paths: IPathMap;

  private adjustedDartPoints: IAdjustedDartPoints;

  constructor(dartPoints: IDartPoints) {
    const adjustedDartPoints = getAdjustedDartPoints(dartPoints);
    this.adjustedDartPoints = adjustedDartPoints;
    const { adjustedInnerPoint0, adjustedInnerPoint1, basePoint } =
      adjustedDartPoints;

    this.models = {
      dart: new models.ConnectTheDots(false, [
        adjustedInnerPoint0,
        basePoint,
        adjustedInnerPoint1,
      ]),
      outline: new models.ConnectTheDots(false, [
        adjustedDartPoints.outerPoint0,
        adjustedInnerPoint0,
        adjustedDartPoints.bisector,
        adjustedInnerPoint1,
        adjustedDartPoints.outerPoint1,
      ]),
    };

    this.paths = {
      dot0: dot(halfPoint(adjustedInnerPoint0, basePoint)),
      dot1: dot(halfPoint(adjustedInnerPoint1, basePoint)),
    };
  }

  public getPoints() {
    return this.adjustedDartPoints;
  }
}

export function getAdjustedDartPoints(
  dartPoints: IDartPoints
): IAdjustedDartPoints {
  const { outerPoint0, innerPoint0, basePoint, innerPoint1, outerPoint1 } =
    dartPoints;

  const { abs, acos, max, PI, sin } = Math;

  const outerDistance0 = measure.pointDistance(basePoint, outerPoint0);
  const outerDistance1 = measure.pointDistance(basePoint, outerPoint1);

  const outerAngle0 = abs(
    angle.ofPointInRadians(basePoint, outerPoint0) -
      angle.ofPointInRadians(basePoint, innerPoint0)
  );
  const outerAngle1 = abs(
    angle.ofPointInRadians(basePoint, outerPoint1) -
      angle.ofPointInRadians(basePoint, innerPoint1)
  );

  const topDistanceSquared =
    solveTriangleSAS(
      outerDistance0,
      outerAngle0 + outerAngle1,
      outerDistance1
    ) ** 2;

  const cornerAngle0 = acos(
    (outerDistance0 ** 2 + topDistanceSquared - outerDistance1 ** 2) /
      (2 * outerDistance0 * topDistanceSquared ** 0.5)
  );
  const dartLength =
    (outerDistance0 / sin(PI - cornerAngle0 - outerAngle0)) * sin(cornerAngle0);

  const adjustedInnerPoint0 = pointAtDistance(
    basePoint,
    innerPoint0,
    dartLength
  );
  const adjustedInnerPoint1 = pointAtDistance(
    basePoint,
    innerPoint1,
    dartLength
  );

  const bisectorLine = new paths.Line(
    basePoint,
    pointAtAngle(
      basePoint,
      (angle.ofPointInRadians(basePoint, adjustedInnerPoint0) +
        angle.ofPointInRadians(basePoint, adjustedInnerPoint1)) /
        2,
      2
    )
  );

  const bisectorLength = max(
    measure.pointDistance(
      basePoint,
      point.fromSlopeIntersection(
        bisectorLine,
        new paths.Line(outerPoint0, adjustedInnerPoint0)
      )
    ),
    measure.pointDistance(
      basePoint,
      point.fromSlopeIntersection(
        bisectorLine,
        new paths.Line(outerPoint1, adjustedInnerPoint1)
      )
    )
  );

  return {
    ...dartPoints,
    adjustedInnerPoint0,
    adjustedInnerPoint1,
    bisector: pointAtDistance(basePoint, bisectorLine.end, bisectorLength),
  };
}
