import {
  angle,
  IModel,
  IModelMap,
  IPathMap,
  IPoint,
  measure,
  models,
  paths,
  point,
} from "makerjs";
import { Curve } from "../helpers/curve";
import { Dart } from "../helpers/dart";
import { dot } from "../helpers/dot";
import { ANGLE_DOWN, pointAtAngle, pointAtDistance } from "../helpers/points";
import { WaistDart } from "../helpers/waistDart";
import { IMeasurements } from "../measurements";
import {
  BALANCE_ANGLE,
  getArmholeDepth,
  getBustWidth,
  getFrontDartWidth,
  getHipWidth,
  getOuterWaistPoint,
  getPatternWidth,
  getShoulderPoints,
  getSideDartWidth,
  getUnderArmWidth,
  getWaistReduction,
  HIP_LENGTH,
  LOWER_WAISTLINE,
} from "./shared";

export class Front implements IModel {
  public models: IModelMap = {};
  public paths: IPathMap = {};
  public layer = "fuchsia";

  constructor(measurements: IMeasurements) {
    const { bust, bustHeight, waistHeight, shoulderWidth, ease } = measurements;
    const { PI, max } = Math;

    const maxWidth = getPatternWidth(measurements);

    const bustPoint: IPoint = [
      maxWidth - getBustWidth(measurements) / 2,
      -bustHeight,
    ];
    this.paths.bustPoint = dot(bustPoint);

    const underArmWidth = getUnderArmWidth(measurements);
    this.paths.bustLine = new paths.Line(bustPoint, [
      maxWidth - max(bust / 4, underArmWidth),
      -bustHeight,
    ]);

    const { upperShoulderPoint, lowerShoulderPoint } =
      getShoulderPoints(measurements);
    const shoulderLength = measure.pointDistance(
      upperShoulderPoint,
      lowerShoulderPoint
    );
    const neckWidth = upperShoulderPoint[0];
    upperShoulderPoint[0] = maxWidth - upperShoulderPoint[0];
    const centerFrontNecklinePoint: IPoint = [maxWidth, -neckWidth - 1 / 2];

    this.models.neckline = new Curve([
      {
        origin: centerFrontNecklinePoint,
        angle: PI,
        distance: (neckWidth / 3) * 2,
      },
      {
        origin: upperShoulderPoint,
        angle: PI / 2,
        distance: neckWidth / 2,
      },
    ]);

    const armholeDepth = getArmholeDepth(measurements);
    const underArmPoint: IPoint = [
      maxWidth - underArmWidth,
      lowerShoulderPoint[1] - armholeDepth,
    ];

    const shoulderPoint: IPoint = [
      underArmPoint[0],
      (lowerShoulderPoint[1] * 3) / 2,
    ];

    const shoulderDartBasePoint = point.add(bustPoint, [0, 1]);

    const shoulderDart = new Dart({
      outerPoint0: upperShoulderPoint,
      innerPoint0: pointAtDistance(
        upperShoulderPoint,
        shoulderPoint,
        shoulderLength / 2
      ),
      basePoint: shoulderDartBasePoint,
      innerPoint1: pointAtDistance(
        shoulderPoint,
        upperShoulderPoint,
        shoulderLength / 2
      ),
      outerPoint1: shoulderPoint,
    });
    this.models.shoulder = shoulderDart;
    const { adjustedInnerPoint0, adjustedInnerPoint1 } =
      shoulderDart.getPoints();

    const halfArmholeHeight =
      shoulderPoint[1] - armholeDepth / 2 - shoulderPoint[1] / 6;
    const dartLengthAtHalfArmhole = measure.pointDistance(
      bustPoint,
      point.fromSlopeIntersection(
        new paths.Line(bustPoint, adjustedInnerPoint0),
        new paths.Line([0, halfArmholeHeight], [1, halfArmholeHeight])
      )
    );
    const dartWidthAtHalfArmhole = measure.pointDistance(
      pointAtDistance(bustPoint, adjustedInnerPoint0, dartLengthAtHalfArmhole),
      pointAtDistance(bustPoint, adjustedInnerPoint1, dartLengthAtHalfArmhole)
    );
    const middleArmholePoint: IPoint = [
      maxWidth - (shoulderWidth / 2 - ease / 4 + dartWidthAtHalfArmhole),
      halfArmholeHeight,
    ];
    this.paths.armholeDot = dot(middleArmholePoint);

    this.models.armhole = new Curve([
      {
        origin: shoulderPoint,
        angle: 0,
        distance: 0,
      },
      {
        origin: middleArmholePoint,
        angle:
          ((angle.ofPointInRadians(shoulderPoint, middleArmholePoint) -
            PI * 2) *
            3 +
            ANGLE_DOWN) /
          4,
        distance: armholeDepth / 3,
      },
      {
        origin: underArmPoint,
        angle: BALANCE_ANGLE + PI,
        distance: armholeDepth / 6,
      },
    ]);

    const waistReduction = getWaistReduction(measurements);
    const outerWaistPoint: IPoint = [
      maxWidth -
        getOuterWaistPoint(measurements) +
        getSideDartWidth(waistReduction),
      -waistHeight,
    ];
    const outerHipPoint: IPoint = [
      maxWidth - getHipWidth(measurements),
      -waistHeight - HIP_LENGTH,
    ];
    this.models.side = new Curve([
      {
        origin: underArmPoint,
        angle: BALANCE_ANGLE - PI / 2,
        distance: (underArmPoint[1] + waistHeight) / 3,
      },
      {
        origin: outerWaistPoint,
        angle: ANGLE_DOWN,
        distance: HIP_LENGTH / 4,
      },
      {
        origin: outerHipPoint,
        angle: ANGLE_DOWN,
        distance: HIP_LENGTH / 3,
      },
    ]);

    const frontDartUpperPoint = pointAtAngle(bustPoint, ANGLE_DOWN, 1);
    const frontDartInnerPoint: IPoint = [
      frontDartUpperPoint[0] + getFrontDartWidth(waistReduction) / 2,
      outerWaistPoint[1],
    ];
    const frontDartOuterPoint = point.add(frontDartInnerPoint, [
      -getFrontDartWidth(waistReduction),
      0,
    ]);
    const frontDartLowerPoint: IPoint = [
      frontDartUpperPoint[0],
      outerHipPoint[1] + HIP_LENGTH / 4,
    ];
    this.paths.dartHipPoint = dot([frontDartLowerPoint[0], outerHipPoint[1]]);

    this.models.frontWaistDart = new WaistDart([
      frontDartInnerPoint,
      frontDartUpperPoint,
      frontDartOuterPoint,
      frontDartLowerPoint,
    ]);

    const centerWaistPoint: IPoint = [maxWidth, outerWaistPoint[1]];
    [
      centerWaistPoint,
      frontDartInnerPoint,
      frontDartOuterPoint,
      outerWaistPoint,
    ].forEach((p, i) => (this.paths[`waistDot${i}`] = dot(p)));

    const lowerCenterWaistPoint = point.add(centerWaistPoint, [
      0,
      -LOWER_WAISTLINE,
    ]);
    const lowerInnerDartPoint = point.fromSlopeIntersection(
      new paths.Line(
        lowerCenterWaistPoint,
        point.add(lowerCenterWaistPoint, [1, 0])
      ),
      new paths.Line(frontDartUpperPoint, frontDartInnerPoint)
    );
    this.models.innerWaistLine = new models.ConnectTheDots(false, [
      lowerCenterWaistPoint,
      lowerInnerDartPoint,
      frontDartInnerPoint,
    ]);

    const lowerOuterDartPoint = point.fromSlopeIntersection(
      new paths.Line(
        lowerCenterWaistPoint,
        point.add(lowerCenterWaistPoint, [1, 0])
      ),
      new paths.Line(frontDartUpperPoint, frontDartOuterPoint)
    );
    const lowerOuterWaistPoint = point.add(outerWaistPoint, [
      0,
      -LOWER_WAISTLINE / 2,
    ]);
    this.models.outerWaistLine = new models.ConnectTheDots(false, [
      frontDartOuterPoint,
      lowerOuterDartPoint,
      lowerOuterWaistPoint,
    ]);

    this.models.seam = new models.ConnectTheDots(false, [
      outerHipPoint,
      [maxWidth, outerHipPoint[1]],
      centerFrontNecklinePoint,
    ]);
  }
}
